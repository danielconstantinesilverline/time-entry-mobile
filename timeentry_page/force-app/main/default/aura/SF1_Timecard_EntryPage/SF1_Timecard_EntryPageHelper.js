({
    /* get date entry object
     * each object has:
     *   day: daysName
     *   click: false/true
     */
    getDayAndDate: function (cmp) {
        const dayAndDate = JSON.stringify(cmp.get("v.entryPageDaysAndDates"));
        return JSON.parse(dayAndDate);
    },

    /* changes the UI by setting the object click attribute to true depends on day */
    toggleDateClicked: function (cmp, dayChosen) {
        const entryPageDaysAndDates = this.getDayAndDate(cmp);
        for (let dayAndDate of entryPageDaysAndDates) {
            if (dayAndDate.day === dayChosen) {
                dayAndDate.click = true;
            } else {
                dayAndDate.click = false;
            }
        }
        cmp.set("v.entryPageDaysAndDates", entryPageDaysAndDates);
    },

    /* changes the indicator of day depends on the day the user choose */
    changeDayDisplay: function (cmp, day) {
        switch (day) {
            case "Sun":
                cmp.set("v.today", "Sunday");
                break;
            case "Mon":
                cmp.set("v.today", "Monday");
                break;
            case "Tue":
                cmp.set("v.today", "Tuesday");
                break;
            case "Wed":
                cmp.set("v.today", "Wednesday");
                break;
            case "Thu":
                cmp.set("v.today", "Thursday");
                break;
            case "Fri":
                cmp.set("v.today", "Friday");
                break;
            default:
                cmp.set("v.today", "Saturday");
        }
    },

    /* iterating through all the timecards
       set the hour and note based on the timecard (if found) */
    changeEntryPageHourAndNotes: function (cmp, dayChosen) {
        const timecardsContainer = this.getTimecardsContainer(cmp);
        const entryPageRole = this.getAssignmentChosen(cmp).pse__Role__c;
        const entryPageMilestone = this.getMilestoneChosen(cmp).Id;

        for (let timecard of timecardsContainer) {
            const timecardRole = timecard.pse__Assignment__r.pse__Role__c;
            const timecardMilestone = timecard.pse__Milestone__c;

            if (timecardRole === entryPageRole && timecardMilestone === entryPageMilestone) {
                this.chooseDayToEntryPageHour(cmp, dayChosen, timecard);
                this.chooseDayToEntryPageHour(cmp, dayChosen, timecard);
                cmp.set("v.entryPageCurrentlyActiveTimecard", timecard);
                break;
            } else {
                this.setEntryPageHour(cmp, 0, "");
                cmp.set("v.entryPageCurrentlyActiveTimecard", {})
            }
        }
    },

    /* set assignment the user choose */
    setAssignmentChosen: function (cmp, selectedAssignmentId, selectedAssignmentRole) {
        const assignmentsContainer = this.getAssignmentsContainer(cmp);
        for (let assignment of assignmentsContainer) {
            const assignmentId = assignment.pse__Project__c;
            const assignmentRole = assignment.pse__Role__c;
            if (assignmentId === selectedAssignmentId && assignmentRole === selectedAssignmentRole) {
                cmp.set("v.entryPageAssignmentChosen", assignment);
            }
        }
    },

    /* fill the milestone list based on the assignment 
       get the milestone that is not closed, set it as a new object with 
       label and value (the milestone id) */
    setMilestonesListBasedOnAssignment: function (cmp, selectedAssignmentId) {
        const milestonesForThisAssignment = [];

        const milestonesContainer = this.getMilestonesContainer(cmp);
        for (let milestone of milestonesContainer) {
            const milestoneAssignmentId = milestone.pse__Project__c;
            const milestoneStatus = milestone.pse__Status__c;

            if (milestoneAssignmentId === selectedAssignmentId && milestoneStatus !== 'Closed') {
                const milestoneDropdownItem = {};
                if ('Parent_Milestone__r' in milestone) {
                    milestoneDropdownItem.label = milestone.Parent_Milestone__r.Name + ' - ' + milestone.Name;
                } else {
                    milestoneDropdownItem.label = milestone.Name;
                }
                milestoneDropdownItem.value = milestone.Id;

                milestonesForThisAssignment.push(milestoneDropdownItem);
            }
        }
        this.sortMilestones(milestonesForThisAssignment);
        cmp.set("v.entryPageMilestonesContainer", milestonesForThisAssignment);
    },

    /* sort the milestones alphabetically */
    sortMilestones: function (milestonesContainer) {
        milestonesContainer.sort(function (firstObj, secondObj) {
            let attributesForFirstObj = firstObj.label
            let attributesForSecondObj = secondObj.label;

            const lowercaseA = attributesForFirstObj.toLowerCase();
            const lowercaseB = attributesForSecondObj.toLowerCase();

            if (lowercaseA < lowercaseB)
                return -1;
            else if (lowercaseA > lowercaseB)
                return 1;
            return 0;
        });
        return;
    },

    /* prompted user to choose milestone before entering time and hour */
    requiredToChooseMilestoneFirst: function (cmp) {
        cmp.set("v.entryPageMilestoneChosenString", "Select milestone:");
        cmp.set("v.entryPageDisabledTimeAndNotes", true);
        cmp.set("v.entryPageDisabledSave", true);
        cmp.set("v.entryPageMilestoneChosen", '');
        this.setEntryPageHour(cmp, 0, "");
    },

    /* set the milestone chosen by the user */
    setMilestoneChosen: function (cmp, selectedMilestoneId) {
        const milestonesContainer = this.getMilestonesContainer(cmp);
        for (let milestone of milestonesContainer) {
            const milestoneId = milestone.Id;
            if (milestoneId === selectedMilestoneId) {
                cmp.set("v.entryPageMilestoneChosen", milestone);
                break;
            }
        }
    },

    /* fill the data for hour and notes based on the timecard and the day */
    chooseDayToEntryPageHour: function (cmp, day, currentTimecard) {
        switch (day) {
            case 'Sun':
                this.setEntryPageHour(cmp, currentTimecard.pse__Sunday_Hours__c, currentTimecard.pse__Sunday_Notes__c);
                break;
            case 'Mon':
                this.setEntryPageHour(cmp, currentTimecard.pse__Monday_Hours__c, currentTimecard.pse__Monday_Notes__c)
                break;
            case 'Tue':
                this.setEntryPageHour(cmp, currentTimecard.pse__Tuesday_Hours__c, currentTimecard.pse__Tuesday_Notes__c)
                break;
            case 'Wed':
                this.setEntryPageHour(cmp, currentTimecard.pse__Wednesday_Hours__c, currentTimecard.pse__Wednesday_Notes__c)
                break;
            case 'Thu':
                this.setEntryPageHour(cmp, currentTimecard.pse__Thursday_Hours__c, currentTimecard.pse__Thursday_Notes__c);
                break;
            case 'Fri':
                this.setEntryPageHour(cmp, currentTimecard.pse__Friday_Hours__c, currentTimecard.pse__Friday_Notes__c);
                break;
            case 'Sat':
                this.setEntryPageHour(cmp, currentTimecard.pse__Saturday_Hours__c, currentTimecard.pse__Saturday_Notes__c)
                break;
        }
    },

    setEntryPageHour: function (cmp, hour, notes) {
        cmp.set("v.entryPageHour", hour);
        cmp.set("v.entryPageNote", notes);
    },

    getTimecardsContainer: function (cmp) {
        const timecardsContainer = JSON.stringify(cmp.get("v.timecardsContainer"));
        return JSON.parse(timecardsContainer);
    },

    getAssignmentsContainer: function (cmp) {
        const assignmentContainer = JSON.stringify(cmp.get("v.assignmentsContainer"));
        return JSON.parse(assignmentContainer)
    },

    getMilestonesContainer: function (cmp) {
        const milestonesContainer = JSON.stringify(cmp.get("v.milestonesContainer"));
        return JSON.parse(milestonesContainer);
    },

    getAssignmentChosen: function (cmp) {
        return cmp.get("v.entryPageAssignmentChosen");
    },

    getMilestoneChosen: function (cmp) {
        return cmp.get("v.entryPageMilestoneChosen");
    },

    /*  get the current active timecard's notes and hours based on day
        check if the timecard is saved/submitted/approved
        if submitted/approved, disable save, and display message to the user
        if saved, but the value of hours and notes is same with what the timecard has, disabled save (only allow save if the value is changed)
    */
    handleEntryPageDisabledSave: function (cmp) {
        const notes = cmp.get("v.entryPageNote");
        const hour = cmp.get("v.entryPageHour");
        let entryPageCurrentlyActiveTimecard = JSON.parse(JSON.stringify(cmp.get("v.entryPageCurrentlyActiveTimecard")));
        const today = cmp.get("v.today");
        this.hideMessageTimecardIsSubmitted(cmp);
        this.hideMessageTimecardIsApproved(cmp);
        // this.allowEnterTimeAndNote(cmp);
        if (entryPageCurrentlyActiveTimecard !== null) {
            let timecardNote = '';
            let timecardHour = '';
            switch (today) {
                case 'Monday':
                    timecardNote = entryPageCurrentlyActiveTimecard.pse__Monday_Notes__c;
                    timecardHour = entryPageCurrentlyActiveTimecard.pse__Monday_Hours__c;
                    break;
                case 'Tuesday':
                    timecardNote = entryPageCurrentlyActiveTimecard.pse__Tuesday_Notes__c;
                    timecardHour = entryPageCurrentlyActiveTimecard.pse__Tuesday_Hours__c;
                    break;
                case 'Wednesday':
                    timecardNote = entryPageCurrentlyActiveTimecard.pse__Wednesday_Notes__c;
                    timecardHour = entryPageCurrentlyActiveTimecard.pse__Wednesdday_Hours__c;
                    break;
                case 'Thursday':
                    timecardNote = entryPageCurrentlyActiveTimecard.pse__Thursday_Notes__c;
                    timecardHour = entryPageCurrentlyActiveTimecard.pse__Thursday_Hours__c;
                    break;
                case 'Friday':
                    timecardNote = entryPageCurrentlyActiveTimecard.pse__Friday_Notes__c;
                    timecardHour = entryPageCurrentlyActiveTimecard.pse__Friday_Hours__c;
                    break;
                case 'Saturday':
                    timecardNote = entryPageCurrentlyActiveTimecard.pse__Saturday_Notes__c;
                    timecardHour = entryPageCurrentlyActiveTimecard.pse__Saturday_Hours__c;
                    break;
                default:
                    timecardNote = entryPageCurrentlyActiveTimecard.pse__Sunday_Notes__c;
                    timecardHour = entryPageCurrentlyActiveTimecard.pse__Sunday_Hours__c;
                    break;
            }
            if (notes === undefined || notes === "" || hour === "" || hour == 0) {
                this.dontAllowSaveEntryPage(cmp);
            } else if (notes === timecardNote && hour === timecardHour) {
                this.dontAllowSaveEntryPage(cmp);
            } else {
                this.allowSaveEntryPage(cmp);
            }
            if (entryPageCurrentlyActiveTimecard.pse__Status__c === "Submitted") {
                this.dontAllowSaveEntryPage(cmp);
                this.showMessageTimecardIsSubmitted(cmp);
                this.hideMessageTimecardIsApproved(cmp);
                this.dontAllowEnterTimeAndNote(cmp);
            }

            if (entryPageCurrentlyActiveTimecard.pse__Status__c === "Approved") {
                this.dontAllowSaveEntryPage(cmp);
                this.hideMessageTimecardIsSubmitted(cmp);
                this.showMessageTimecardIsApproved(cmp);
                this.dontAllowEnterTimeAndNote(cmp);
            }

        } else {
            if (notes === undefined || notes === "" || hour === "" || hour == 0) {
                this.dontAllowSaveEntryPage(cmp);
            } else {
                this.allowSaveEntryPage(cmp);
            }
        }
    },

    showMessageTimecardIsSubmitted: function (cmp) {
        cmp.set("v.entryPageTimecardIsAlreadySubmitted", true);
    },

    hideMessageTimecardIsSubmitted: function (cmp) {
        cmp.set("v.entryPageTimecardIsAlreadySubmitted", false);
    },

    showMessageTimecardIsApproved: function (cmp) {
        cmp.set("v.entryPageTimecardIsAlreadyApproved", true);
    },

    hideMessageTimecardIsApproved: function (cmp) {
        cmp.set("v.entryPageTimecardIsAlreadyApproved", false);
    },


    allowSaveEntryPage: function (cmp) {
        cmp.set("v.entryPageDisabledSave", false);
    },

    dontAllowSaveEntryPage: function (cmp) {
        cmp.set("v.entryPageDisabledSave", true);
    },

    allowEnterTimeAndNote: function (cmp) {
        cmp.set("v.entryPageDisabledTimeAndNotes", false);
    },

    dontAllowEnterTimeAndNote: function (cmp) {
        cmp.set("v.entryPageDisabledTimeAndNotes", true);
    }
})