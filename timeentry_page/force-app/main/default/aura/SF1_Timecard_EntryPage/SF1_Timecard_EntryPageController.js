({
    /*
     * handle when user changes the day from the navigation in entry page
     * change the displayed day
     * look through the timecard, see if user has timecard for today with the assignment and milestone
     * set hour and note
     */
    changeDay: function (cmp, event, helper) {
        const dayChosen = event.getSource().get("v.value");
        cmp.set("v.entryPageDayChosen", dayChosen);

        helper.toggleDateClicked(cmp, dayChosen);
        helper.changeDayDisplay(cmp, dayChosen);
        helper.changeEntryPageHourAndNotes(cmp, dayChosen);
        helper.handleEntryPageDisabledSave(cmp);
    },

    /*
     * handle when user changes the project from drop down in entry page
     * set the chosen project, prefill the milestones dropdown
     * required user to choose milestone first 
     */
    handleProjectChange: function (cmp, event, helper) {
        const selectedAssignment = event.getParam("value");

        let [selectedAssignmentId, ...selectedAssignmentRole] = selectedAssignment.split(" ")
        selectedAssignmentRole = selectedAssignmentRole.join(" ");

        helper.setAssignmentChosen(cmp, selectedAssignmentId, selectedAssignmentRole);
        helper.setMilestonesListBasedOnAssignment(cmp, selectedAssignmentId);
        helper.requiredToChooseMilestoneFirst(cmp);
        helper.handleEntryPageDisabledSave(cmp);

        helper.hideMessageTimecardIsSubmitted(cmp);
        helper.hideMessageTimecardIsApproved(cmp);
        helper.dontAllowEnterTimeAndNote(cmp);
        // cmp.set("v.entryPageTimecardIsAlreadySubmitted", false);
    },

    /*
     * handle when user changes the milestone from drop down in entry page
     * set the chosen milestone
     * look for the timecard (set the hour and note from timecard)
     */
    handleMilestoneChange: function (cmp, event, helper) {
        const milestoneChosen = event.getParam("value");

        helper.setMilestoneChosen(cmp, milestoneChosen);
        const day = cmp.get("v.entryPageDayChosen");
        helper.allowEnterTimeAndNote(cmp);
        helper.changeEntryPageHourAndNotes(cmp, day);
        helper.handleEntryPageDisabledSave(cmp);

    },

    /*
     * handle onChange in notes
     * check if hour or note is empty
     */
    handleChangeNotes: function (cmp, event, helper) {
        helper.handleEntryPageDisabledSave(cmp);
    },

    /*
     * handle onChange in hours
     * check if hour or note is empty
     */
    handleChangeHours: function (cmp, event, helper) {
        helper.handleEntryPageDisabledSave(cmp);
    },

    /*
     * handle subtracting hour
     */
    handleSubstract: function (cmp, event, helper) {
        const hour = cmp.get("v.entryPageHour")
        if (hour == 0) {
            return;
        }
        const updatedHour = Number(hour) - 0.25;
        cmp.set("v.entryPageHour", updatedHour);
        helper.handleEntryPageDisabledSave(cmp);
    },

    /* handle adding hour */
    handleAddition: function (cmp, event, helper) {
        const hour = cmp.get("v.entryPageHour")
        if (hour == 24) {
            return;
        }
        const updatedHour = Number(hour) + 0.25;
        cmp.set("v.entryPageHour", updatedHour);
        helper.handleEntryPageDisabledSave(cmp);
    },

    /* handle save button */
    save: function (cmp) {
        const updateTimecard = cmp.get("v.updateTimecard");
        $A.enqueueAction(updateTimecard);
    },

    /* handle to go back to landing page */
    toggleLandingPageAndEntryPage: function (cmp) {
        const toggleLandingPageAndEntryPage = cmp.get("v.toggleLandingPageAndEntryPage");
        $A.enqueueAction(toggleLandingPageAndEntryPage);
    },

})