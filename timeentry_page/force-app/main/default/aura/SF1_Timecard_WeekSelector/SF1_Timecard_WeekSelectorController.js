({
    goToPrevWeek: function (cmp, event, helper) {
        const goToPrevWeek = cmp.get("v.goToPrevWeek");
        $A.enqueueAction(goToPrevWeek);
    },

    goToNextWeek: function (cmp, event, helper) {
        const goToNextWeek = cmp.get("v.goToNextWeek");
        $A.enqueueAction(goToNextWeek);
    }
})