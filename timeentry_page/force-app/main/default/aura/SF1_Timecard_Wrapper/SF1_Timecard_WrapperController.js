({
    /* set today's date. use today's date to get the dates from Sunday to Saturday for that week.
       set the user id and use it to get the assignments, milestones, and timecards for that user
    */
    doInit: function (cmp, event, helper) {
        cmp.set("v.todayDate", new Date());
        cmp.set("v.prevOrNextDate", new Date()); //test
        helper.handleDate(cmp, new Date());
        helper.setUserId(cmp);
    },

    /*  change the week to previous week, set the today date to last week,
     *  fetch the data for last week */
    goToPrevWeek: function (cmp, event, helper) {
        const todayDate = cmp.get("v.prevOrNextDate");
        const pastWeekDate = todayDate.getDate() - 7;
        todayDate.setDate(pastWeekDate);

        cmp.set("v.prevOrNextDate", todayDate);
        helper.handleDate(cmp, todayDate);
        helper.refreshPage(cmp);
    },

    /*  change the week to next week, set the today date to next week,
     *  fetch the data for next week */
    goToNextWeek: function (cmp, event, helper) {
        const todayDate = cmp.get("v.prevOrNextDate");
        const nextWeekDate = todayDate.getDate() + 7;
        todayDate.setDate(nextWeekDate);

        cmp.set("v.prevOrNextDate", todayDate);
        helper.handleDate(cmp, todayDate);
        helper.refreshPage(cmp);
    },

    /* switch between landing page and enter time page */
    toggleLandingPageAndEntryPage: function (cmp) {
        cmp.set("v.showLandingPage", !cmp.get("v.showLandingPage"));
        cmp.set("v.showEntryPage", !cmp.get("v.showEntryPage"));
    },

    /* for saving timecard, there are two cases:
     *  1. User do not have timecards for that assignment and that milestone --> create new timecard
     *  2. User has timecards for that assignment and that milestone --> modify the current timecard */
    saveTimecard: function (cmp, event, helper) {
        cmp.set("v.entryPageIsSaving", true);
        let timecard = {};
        const timecardsContainer = helper.getTimecardsContainer(cmp);
        if (timecardsContainer.length !== 0) { //if we have timecards, we tried to find the timecard for that assignment's milestone
            const assignmentChosenId = helper.getChosenAssignmentId(cmp);
            const milestoneChosenId = helper.getChosenMilestoneId(cmp);
            timecard = helper.findCorrectTimecard(timecardsContainer, timecard, assignmentChosenId, milestoneChosenId);
        }

        let userHasTimecard = true;
        if (Object.keys(timecard).length === 0) { //if there is no timecard, we generate one.
            helper.generateTimecard(cmp, timecard);
            userHasTimecard = false;
        }
        helper.modifyTimecard(cmp, timecard) //we modify the timecard with hour and notes from entry page
        timecard.pse__Status__c = "Saved";
        const timecardClone = Object.assign({}, timecard);

        // if it's a new timecard we just generate
        // we delete the id from the second timecard
        // if (!userHasTimecard) {
        if (!userHasTimecard) {
            delete timecardClone.Id;
        }

        const jsonOriginal = helper.createJSONTimecard(timecard)
        const jsonObjects = helper.createJSONTimecard(timecardClone);

        const modifiedTimecard = timecardsContainer.filter(item => {
            return item.Id != timecard.Id;
        }); //we remove the timecard that is being modified from the timecard container, we will insert the new one into the container
        const assignmentsContainer = helper.getAssignmentsContainer(cmp);
        let correctAssignment = ''; //we're looking for assignment that is being saved
        for (let assignment of assignmentsContainer) {
            if (timecard.pse__Project__c === assignment.pse__Project__c && assignment.Id === timecard.pse__Assignment__c) {
                correctAssignment = assignment;
                break;
            }
        }

        const upsertTimecardsList = cmp.get("c.upsertTimecardsList");
        upsertTimecardsList.setParams({
            "objectType": "pse__Timecard_Header__c",
            "externalIdField": "Id",
            "jsonObjects": jsonObjects,
            "jsonOriginals": jsonOriginal,
        })
        upsertTimecardsList.setCallback(this, function (response) {
            const stateCreateAssignment = response.getState();
            const returnValue = JSON.parse(response.getReturnValue());
            if ('errorCode' in returnValue) {
                helper.showToast('Failed to save timecard', returnValue.message);
                cmp.set("v.entryPageIsSaving", false);
                cmp.set("v.showLandingPage", !cmp.get("v.showLandingPage"));
                cmp.set("v.showEntryPage", !cmp.get("v.showEntryPage"));
                helper.checkAllowSubmit(cmp);
                helper.stopLoadingIndicator(cmp);
                return;
            }

            if (stateCreateAssignment === 'SUCCESS') {
                const recordsFromReturnValue = returnValue.records;
                timecard = recordsFromReturnValue[0];
                helper.calculateTotalHourTimecard(timecard); //calculate total hour on that timecard
                modifiedTimecard.push(timecard); //pushing the new modified timecard into container
                cmp.set("v.timecardsContainer", modifiedTimecard);

                helper.calculateHoursBasedOnDayOfTheWeek(cmp, modifiedTimecard); //calculate based on days for weekly view
                helper.calculateAuraAssignment(cmp, correctAssignment); //calculate for project view

                const updateAssignment = helper.calculateMilestoneInsideAssignment(cmp, correctAssignment, timecard, assignmentsContainer); //calculate milestones hour under the assignment
                cmp.set("v.assignmentsContainer", updateAssignment);

                helper.calculateTotalHoursDisplay(cmp, modifiedTimecard); //calculating total hours saved, submitted, etc.
                cmp.set("v.entryPageIsSaving", false);
                cmp.set("v.showLandingPage", !cmp.get("v.showLandingPage"));
                cmp.set("v.showEntryPage", !cmp.get("v.showEntryPage"));
                helper.showToast('Success', 'Timecard has been saved successfully.');
            } else {
                console.log("Failed saving timecard with a state: " + stateCreateAssignment);
                cmp.set("v.entryPageIsSaving", true);
                helper.showToast('Failed', 'Failed to save timecard.');
            }
            helper.checkAllowSubmit(cmp);
            helper.stopLoadingIndicator(cmp);
        });
        $A.enqueueAction(upsertTimecardsList)
    },

    /* change all timecards status to submit and then send to the server 
     */
    submitTimecard: function (cmp, event, helper) {
        helper.showLoadingIndicator(cmp);
        const timecardsContainer = helper.changeAllTimecardsStatusToSubmit(cmp);
        const timecardObject = Array.from(timecardsContainer);

        const jsonObjects = JSON.stringify(timecardObject);
        const jsonOriginal = JSON.stringify(timecardsContainer)

        const upsertTimecardsList = cmp.get("c.upsertTimecardsList");
        upsertTimecardsList.setParams({
            "objectType": "pse__Timecard_Header__c",
            "externalIdField": "Id",
            "jsonObjects": jsonObjects,
            "jsonOriginals": jsonOriginal,
        })
        upsertTimecardsList.setCallback(this, function (response) {
            const stateCreateAssignment = response.getState();
            if (stateCreateAssignment === 'SUCCESS') {
                cmp.set("v.timecardsContainer", timecardsContainer)
                cmp.set("v.totalHoursUnsubmitted", 0);
                cmp.set("v.totalHoursSubmitted", cmp.get("v.totalHoursThisWeek"));
                helper.showToast('Success', 'Timecard has been submitted successfully.');
            } else {
                console.log("Failed updating timecards with a state: " + stateCreateAssignment);
                helper.showToast('Failed', 'Failed to submit timecard.');
            }
            helper.checkAllowSubmit(cmp);
            helper.stopLoadingIndicator(cmp);
        });
        $A.enqueueAction(upsertTimecardsList)
    },

    /* toggle the add project page, everytime we toggle, we reset the data */
    toggleAddProjectPage: function (cmp) {
        cmp.set("v.addProjectPageHasListOfProjects", false);
        cmp.set("v.showAddProjectPage", !cmp.get("v.showAddProjectPage"));
        cmp.set("v.addProjectPageAssignmentNameTyped", "");
        cmp.set("v.addProjectPageRoleChosen", "--None--");
        cmp.set("v.addProjectPageAssignmentChosen", "");
        cmp.find("selectRole").set("v.value", "--None--");
    },

    /* reload page */
    refreshPage: function (cmp, event, helper) {
        helper.refreshPage(cmp);
    },

})