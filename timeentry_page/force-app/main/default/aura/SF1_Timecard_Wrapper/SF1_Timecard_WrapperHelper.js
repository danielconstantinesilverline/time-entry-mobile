({
    /* starts by getting the sunday date of the week
     * then set dates for one week
     */
    handleDate: function (cmp, todayDate) {
        const sundayDate = new Date(todayDate.setDate(todayDate.getDate() - todayDate.getDay()));

        const dateRangeThisWeek = [this.getDateRange(sundayDate)];
        const startAndEndDate = [this.getStartAndEndDate(sundayDate)];

        this.setDatesForOneWeek(sundayDate, dateRangeThisWeek, startAndEndDate);
        this.setDatesForEntryPage(cmp, startAndEndDate);
        this.setWeeklyString(cmp, dateRangeThisWeek)
        this.setStartDate(cmp, startAndEndDate);
        this.setEndDate(cmp, startAndEndDate);
        this.setStartAndEndDateList(cmp, startAndEndDate);
    },

    /* generates date for one week starting with the sunday date */
    setDatesForOneWeek: function (sundayDate, dateRangeThisWeek, startAndEndDate) {
        const eachDate = sundayDate;
        while (eachDate.setDate(eachDate.getDate() + 1) && eachDate.getDay() !== 0) {
            const eachDayInTheWeekDate = new Date(eachDate);
            dateRangeThisWeek.push(this.getDateRange(eachDayInTheWeekDate));
            startAndEndDate.push(this.getStartAndEndDate(eachDayInTheWeekDate));
        }
    },

    /* set attributes needed for enter time page */
    setDatesForEntryPage: function (cmp, startAndEndDate) {
        const days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
        const dateForEntryPage = [];
        for (let i = 0; i < startAndEndDate.length; i++) {
            dateForEntryPage.push(this.getEntryPageDaysAndDates(days[i], startAndEndDate[i]));
        }
        cmp.set("v.entryPageDaysAndDates", dateForEntryPage);
    },

    /* creating object date for enter time page that will be used for displaying which date is active and what not */
    getEntryPageDaysAndDates: function (day, dateString) {
        const dateStringSplitted = dateString.split("-");
        const month = dateStringSplitted[1];
        const date = dateStringSplitted[2];

        const obj = {};
        obj['day'] = day;
        obj['date'] = month + "/" + date;
        obj['click'] = false;

        return obj;
    },

    /* output: 05/12/2019 - 05/19/2019 */
    setWeeklyString: function (cmp, dateRangeThisWeek) {
        const weeklyDateString = dateRangeThisWeek[0] + " - " + dateRangeThisWeek[6];
        cmp.set("v.weeklyDateString", weeklyDateString);
    },

    /* initialize user id and call reload page */
    setUserId: function (cmp) {
        const getResourceContact = cmp.get("c.getResourceContact");
        getResourceContact.setCallback(this, function (response) {
            const stateGetResourceContact = response.getState();
            const userContact = response.getReturnValue();
            const userId = userContact.Id;
            if (stateGetResourceContact === 'SUCCESS') {
                cmp.set("v.userId", userId);
                this.refreshPage(cmp);
            } else {
                console.log("Failed getting user contact with a state: " + stateGetResourceContact);
            }
        });
        $A.enqueueAction(getResourceContact);
    },

    /* reload the page by fetching assignments -> timecards -> milestones
     * using the timecards, we calculate total hours for each assignment and days.
     * also calculate total hours for milestones using timecards
     */
    refreshPage: function (cmp) {
        this.showLoadingIndicator(cmp);

        const assignmentStartDate = cmp.get("v.startDate");
        const assignmentEndDate = cmp.get("v.endDate");
        const userId = cmp.get("v.userId");

        const getAssignments = cmp.get("c.getAssignments");
        getAssignments.setParams({
            "resource": userId,
            "startDate": assignmentStartDate,
            "endDate": assignmentEndDate
        })
        getAssignments.setCallback(this, function (response) {
            const stateGetAssignments = response.getState();
            const assignmentsContainer = JSON.parse(response.getReturnValue());

            if (stateGetAssignments === 'SUCCESS') {
                assignmentsContainer.map(item => {
                    item.aura__assignment__name = item.Name.replace(item.pse__Resource__r.Name, item.pse__Role__c) + ' ($' + item.pse__Bill_Rate__c + ')';
                }); //creating new attribute for UI purpose
            } else {
                console.log("Failed getting all assignment with a state: " + stateGetAssignments);
            }

            const getTimecards = cmp.get("c.getTimecards");
            getTimecards.setParams({
                "startDate": assignmentStartDate,
                "endDate": assignmentEndDate,
                "resourceId": userId
            });
            getTimecards.setCallback(this, function (response) {
                const stateGetTimecards = response.getState();
                const timecardsContainer = JSON.parse(response.getReturnValue());
                if (stateGetTimecards === 'SUCCESS') {
                    cmp.set("v.timecardsContainer", timecardsContainer);
                    this.calculateTotalHoursAssignments(cmp, assignmentsContainer, timecardsContainer); //calculating total hours for assignments using all timecards
                } else {
                    console.log("Failed getting all timecards a state: " + stateGetTimecards);
                }

                const projectIds = assignmentsContainer.map(item => {
                    return item.pse__Project__c;
                }); //get the projectIds forom assignment and use it to get milestones

                const getChildMilestones = cmp.get("c.getChildMilestonesArr");
                getChildMilestones.setParams({
                    "projectIds": projectIds
                });
                getChildMilestones.setCallback(this, function (response) {
                    const stateGetChildMilestones = response.getState();
                    const tempMilestonesContainer = JSON.parse(response.getReturnValue());
                    const milestonesContainer = this.trimmingMilestonesContainer(tempMilestonesContainer);

                    if (stateGetChildMilestones === 'SUCCESS') {
                        this.calculateTotalHoursMilestones(cmp, assignmentsContainer, milestonesContainer, timecardsContainer) //calculating total hours related to the milestone using timecards 
                    } else {
                        console.log("Failed getting all the milestones with a state: " + stateGetChildMilestones);
                    }

                    this.calculateHoursBasedOnDayOfTheWeek(cmp, timecardsContainer); //calculating total hours for weekly view
                    cmp.set("v.entryPageIsSaving", false);
                    cmp.set("v.landingPageIsLoading", true);
                });
                $A.enqueueAction(getChildMilestones);
            });
            $A.enqueueAction(getTimecards);
        });
        $A.enqueueAction(getAssignments);
    },

    /* start calculating total hours for each assignments 
     * and calculating total hours submitted, saved, approved 
     */
    calculateTotalHoursAssignments: function (cmp, assignmentsContainer, timecardsContainer) {
        const totalHoursWeek = {
            totalHourThisWeek: 0,
            totalSavedHoursThisWeek: 0,
            totalSubmittedHoursThisWeek: 0,
            totalApprovedHoursThisWeek: 0
        }
        for (let assignment of assignmentsContainer) {
            const assignmentId = assignment.Id;

            const totalHoursAssignment = {
                totalHours: 0,
                sundayHours: 0,
                mondayHours: 0,
                tuesdayHours: 0,
                wednesdayHours: 0,
                thursdayHours: 0,
                fridayHours: 0,
                saturdayHours: 0
            };

            this.calculateAssignmentHoursFromEachTimecard(assignmentId, timecardsContainer, totalHoursWeek, totalHoursAssignment);
            this.setAttributeHoursForEachAssignment(assignment, totalHoursAssignment);

            totalHoursWeek.totalHourThisWeek += Number(totalHoursAssignment.totalHours);
        }
        cmp.set("v.assignmentsContainer", assignmentsContainer);
        this.setTimeDisplay(cmp, totalHoursWeek);
        this.checkAllowSubmit(cmp);
        return;
    },

    /* find timecard correspond to the assignment and then calculate the hours */
    calculateAssignmentHoursFromEachTimecard: function (assignmentId, timecardsContainer, totalHoursWeek, totalHoursAssignment) {
        for (let timecard of timecardsContainer) {
            if (timecard.pse__Assignment__c === assignmentId) {
                this.switchCalculateHours(totalHoursWeek, timecard.pse__Status__c, timecard.pse__Total_Hours__c);
                this.setAssignmentHourWeekly(totalHoursAssignment, timecard);
            }
        }
        return;
    },

    /* calculate the total hours of submitted, saved, or approved */
    switchCalculateHours: function (totalHoursWeek, status, hour) {
        switch (status) {
            case "Saved":
                totalHoursWeek.totalSavedHoursThisWeek += Number(hour);
                break;
            case "Submitted":
                totalHoursWeek.totalSubmittedHoursThisWeek += Number(hour);
                break;
            default:
                totalHoursWeek.totalApprovedHoursThisWeek += Number(hour);
                totalHoursWeek.totalSubmittedHoursThisWeek += Number(hour);
                break;
        }
        return;
    },

    /* calculate the hour for the assignment for days and total*/
    setAssignmentHourWeekly: function (totalHoursAssignment, timecard) {
        totalHoursAssignment.totalHours += Number(timecard.pse__Total_Hours__c);
        totalHoursAssignment.sundayHours += Number(timecard.pse__Sunday_Hours__c);
        totalHoursAssignment.mondayHours += Number(timecard.pse__Monday_Hours__c);
        totalHoursAssignment.tuesdayHours += Number(timecard.pse__Tuesday_Hours__c);
        totalHoursAssignment.wednesdayHours += Number(timecard.pse__Wednesday_Hours__c);
        totalHoursAssignment.thursdayHours += Number(timecard.pse__Thursday_Hours__c);
        totalHoursAssignment.fridayHours += Number(timecard.pse__Friday_Hours__c);
        totalHoursAssignment.saturdayHours += Number(timecard.pse__Saturday_Hours__c);
        return;
    },

    /* using the hours we just calculate, we assign those as attributes for each assignment */
    setAttributeHoursForEachAssignment: function (assignment, totalHoursAssignment) {
        assignment.aura_total_hours_sunday = totalHoursAssignment.sundayHours;
        assignment.aura_total_hours_monday = totalHoursAssignment.mondayHours;
        assignment.aura_total_hours_tuesday = totalHoursAssignment.tuesdayHours;
        assignment.aura_total_hours_wednesday = totalHoursAssignment.wednesdayHours;
        assignment.aura_total_hours_thursday = totalHoursAssignment.thursdayHours;
        assignment.aura_total_hours_friday = totalHoursAssignment.fridayHours;
        assignment.aura_total_hours_saturday = totalHoursAssignment.saturdayHours;

        assignment.aura_total_hours_assignment = (assignment.aura_total_hours_sunday + assignment.aura_total_hours_monday + assignment.aura_total_hours_tuesday + assignment.aura_total_hours_wednesday + assignment.aura_total_hours_thursday + assignment.aura_total_hours_friday + assignment.aura_total_hours_saturday);
        return;
    },

    setTimeDisplay: function (cmp, totalHoursWeek) {
        cmp.set("v.totalHoursThisWeek", totalHoursWeek.totalHourThisWeek);
        cmp.set("v.totalHoursUnsubmitted", totalHoursWeek.totalSavedHoursThisWeek);
        cmp.set("v.totalHoursSubmitted", totalHoursWeek.totalSubmittedHoursThisWeek);
        cmp.set("v.totalHoursApproved", totalHoursWeek.totalApprovedHoursThisWeek);
    },

    checkAllowSubmit: function (cmp) {
        const savedHours = cmp.get("v.totalHoursUnsubmitted");
        if (savedHours > 0) {
            cmp.set("v.landingPageDisabledSubmit", false);
        } else {
            cmp.set("v.landingPageDisabledSubmit", true)
        }
    },

    /* calculating all milestones hours, and also set the attribute each milestones */
    calculateTotalHoursMilestones: function (cmp, assignmentsContainer, milestonesContainer, timecardsContainer) {
        for (let assignment of assignmentsContainer) {
            const milestonesOfAssignment = []

            for (let milestone of milestonesContainer) {
                if (assignment.pse__Project__c !== milestone.pse__Project__c)
                    continue;

                const milestoneId = milestone.Id;
                const milestonesRoles = [];

                this.calculateMilestoneHoursFromEachTimecard(timecardsContainer, milestoneId, milestonesRoles);
                this.setAttributeHoursForEachMilestone(milestone, milestonesRoles);

                milestonesOfAssignment.push(milestone);
            }

            this.sortMilestones(milestonesOfAssignment); //sort the milestone aplhabetically for UI purpose
            assignment['milestonesOfAssignment'] = milestonesOfAssignment;
        }
        cmp.set("v.milestonesContainer", milestonesContainer);

        const trimmedAssignmentsContainer = this.trimmingAssignmentsContainer(assignmentsContainer);
        cmp.set("v.assignmentsContainer", trimmedAssignmentsContainer)
        return;
    },

    /* users can be assigned same project with different role. 
     * this function is to check each milestone if there is timecard for that milestone but with multiple roles
     * and calculate accordingly 
     */
    calculateMilestoneHoursFromEachTimecard: function (timecardsContainer, milestoneId, milestonesRoles) {
        for (let timecard of timecardsContainer) {
            const milestoneRole = {};
            milestoneRole.value = 0;
            if (milestoneId === timecard.pse__Milestone__c) {
                this.addValueToTheObj(milestoneRole, timecard);
            }

            if (milestoneRole.value != 0) {
                milestonesRoles.push(milestoneRole);
            }
        }
    },

    /* add new custom attributes associated with role for each milestone */
    addValueToTheObj: function (milestoneRole, timecard) {
        milestoneRole.thisRole = timecard.pse__Assignment__r.pse__Role__c;
        milestoneRole.value = timecard.pse__Total_Hours__c;
        milestoneRole.sunday = timecard.pse__Sunday_Hours__c;;
        milestoneRole.monday = timecard.pse__Monday_Hours__c;
        milestoneRole.tuesday = timecard.pse__Tuesday_Hours__c;
        milestoneRole.wednesday = timecard.pse__Wednesday_Hours__c;
        milestoneRole.thursday = timecard.pse__Thursday_Hours__c;
        milestoneRole.friday = timecard.pse__Friday_Hours__c;
        milestoneRole.saturday = timecard.pse__Saturday_Hours__c;
        return;
    },

    /* we set the milestones name for UI purpose */
    setAttributeHoursForEachMilestone: function (milestone, milestonesRoles) {
        milestone.rolesMilestone = milestonesRoles;
        try {
            if ('Parent_Milestone__r' in milestone) {
                milestone.aura_name = milestone.Parent_Milestone__r.Name + ' - ' + milestone.Name
            } else {
                milestone.aura_name = milestone.Name;
            }
        } catch (error) {
            console.log(error)
        }
        return;
    },

    /* sort the milestones alphabetically */
    sortMilestones: function (milestonesContainer) {
        milestonesContainer.sort(function (firstObj, secondObj) {
            let attributesForFirstObj = firstObj.aura_name;
            let attributesForSecondObj = secondObj.aura_name;

            const lowercaseA = attributesForFirstObj.toLowerCase();
            const lowercaseB = attributesForSecondObj.toLowerCase();

            if (lowercaseA < lowercaseB)
                return -1;
            else if (lowercaseA > lowercaseB)
                return 1;
            return 0;
        });
        return;
    },

    /* calculate the hours for each day using all timecards */
    calculateHoursBasedOnDayOfTheWeek: function (cmp, timecardsContainer) {
        const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        const dayOfTheWeekWithTotalHours = [];

        for (let i = 0; i < days.length; i++) {
            const dayObj = {};
            let sum = 0;
            for (let j = 0; j < timecardsContainer.length; j++) {
                let pse = "pse__" + days[i] + "_Hours__c";
                sum += Number(Number(timecardsContainer[j][pse]));
            }
            dayObj.sum = sum;
            dayObj.Name = days[i];
            dayOfTheWeekWithTotalHours.push(dayObj);
        }
        cmp.set("v.dayOfTheWeek", dayOfTheWeekWithTotalHours);
        return;
    },


    /* return all the timecards with status 'Submitted' */
    changeAllTimecardsStatusToSubmit: function (cmp) {
        const timecardsContainerString = JSON.stringify(cmp.get("v.timecardsContainer"));
        const timecardsContainer = JSON.parse(timecardsContainerString);

        for (let timecard of timecardsContainer) {
            timecard.pse__Status__c = "Submitted";
        }

        return timecardsContainer;
    },

    /* find correct timecard from the assignment id and milestone id */
    findCorrectTimecard: function (timecardsContainer, timecard, assignmentId, milestoneId) {
        for (let eachTimecard of timecardsContainer) {
            const timecardAssignmentId = eachTimecard.pse__Assignment__c;
            const timecardMilestoneId = eachTimecard.pse__Milestone__c;
            if (timecardAssignmentId === assignmentId && timecardMilestoneId === milestoneId) {
                timecard = eachTimecard;
                return timecard;
            }
        }

        return {};
    },

    /* create timecards for user: fill with all attributes from assignments and milestones */
    generateTimecard: function (cmp, timecard) {
        this.fillNewTimecardWithAttribute(timecard);
        this.fillNewTimecardWithStartEndDates(cmp, timecard);

        const milestoneChosen = cmp.get("v.entryPageMilestoneChosen");
        const pse__Milestone__r = Object.assign({}, milestoneChosen); //create a copy of the milestone
        this.removeCustomAttributeFromMilestone(pse__Milestone__r);
        this.fillNewTimecardWithMilestone(timecard, pse__Milestone__r);

        const assignmentChosen = cmp.get("v.entryPageAssignmentChosen");
        const assignmentClone = Object.assign({}, assignmentChosen); //create a copy of assignment

        this.removeCustomAttributeFromAssignment(assignmentClone); //remove uneccesarry attributes for assignments 

        timecard.pse__Project__c = assignmentClone.pse__Project__c;
        timecard.pse__Project__r = {};
        timecard.pse__Resource__c = assignmentClone.pse__Resource__c;
        timecard.pse__Billable__c = assignmentClone.pse__Is_Billable__c;
        timecard.pse__Assignment__c = assignmentClone.Id;
        timecard.pse__Assignment__r = {};
        return;
    },

    fillNewTimecardWithAttribute: function (timecard) {
        timecard.pse__Sunday_Hours__c = 0;
        timecard.pse__Sunday_Notes__c = "";
        timecard.pse__Monday_Hours__c = 0;
        timecard.pse__Monday_Notes__c = "";
        timecard.pse__Tuesday_Hours__c = 0;
        timecard.pse__Tuesday_Notes__c = "";
        timecard.pse__Wednesday_Hours__c = 0;
        timecard.pse__Wednesday_Notes__c = "";
        timecard.pse__Thursday_Hours__c = 0;
        timecard.pse__Thursday_Notes__c = "";
        timecard.pse__Friday_Hours__c = 0;
        timecard.pse__Friday_Notes__c = "";
        timecard.pse__Saturday_Hours__c = 0;
        timecard.pse__Saturday_Notes__c = "";
        timecard.pse__Total_Hours__c = 0;
        timecard.pse__Status__c = "Saved";
        timecard.Id = (new Date().getTime());
        timecard.Page__c = 'sl_timeentrypage';
    },

    fillNewTimecardWithStartEndDates: function (cmp, timecard) {
        const startAndEndDateListString = JSON.stringify(cmp.get("v.startAndEndDateList"));
        const startAndEndDateList = JSON.parse(startAndEndDateListString);
        const startDate = startAndEndDateList[0];
        const endDate = startAndEndDateList[6];

        timecard.pse__End_Date__c = endDate;
        timecard.pse__Start_Date__c = startDate;
    },

    fillNewTimecardWithMilestone: function (timecard, pse__Milestone__r) {
        timecard.pse__Milestone__r = pse__Milestone__r;
        timecard.pse__Milestone__c = pse__Milestone__r.Id;

        timecard.pse__Milestone__r.Parent_Milestone__r = this.stringifyAndParseData(pse__Milestone__r.Parent_Milestone__r)
        timecard.pse__Milestone__r.attributes = this.stringifyAndParseData(pse__Milestone__r.attributes)
        timecard.pse__Milestone__r.attributes = this.stringifyAndParseData(pse__Milestone__r.pse__Project__r);
    },

    removeCustomAttributeFromMilestone: function (pse__Milestone__r) {
        delete pse__Milestone__r.rolesMilestone;
        delete pse__Milestone__r.aura_name; //removing this attribute for displaying name in the UI
    },

    removeCustomAttributeFromAssignment: function (pse__Assignment) {
        delete pse__Assignment.aura_total_hours_assignment;
        delete pse__Assignment.aura_total_hours_sunday;
        delete pse__Assignment.aura_total_hours_monday;
        delete pse__Assignment.aura_total_hours_tuesday;
        delete pse__Assignment.aura_total_hours_wednesday;
        delete pse__Assignment.aura_total_hours_thursday;
        delete pse__Assignment.aura_total_hours_friday;
        delete pse__Assignment.aura_total_hours_saturday;
        delete pse__Assignment.aura__assignment__name;
    },

    /* get the data from entry page, and modify timecard using that data. */
    modifyTimecard: function (cmp, timecard) {
        const hour = cmp.get("v.entryPageHour");
        const notes = cmp.get("v.entryPageNote");
        const day = cmp.get("v.entryPageDayChosen");
        switch (day) {
            case 'Mon':
                timecard.pse__Monday_Hours__c = hour;
                timecard.pse__Monday_Notes__c = notes;
                break;
            case 'Tue':
                timecard.pse__Tuesday_Hours__c = hour;
                timecard.pse__Tuesday_Notes__c = notes;
                break;
            case 'Wed':
                timecard.pse__Wednesday_Hours__c = hour;
                timecard.pse__Wednesday_Notes__c = notes;
                break;
            case 'Thu':
                timecard.pse__Thursday_Hours__c = hour;
                timecard.pse__Thursday_Notes__c = notes;
                break;
            case 'Fri':
                timecard.pse__Friday_Hours__c = hour;
                timecard.pse__Friday_Notes__c = notes;
                break;
            case 'Sat':
                timecard.pse__Saturday_Hours__c = hour;
                timecard.pse__Saturday_Notes__c = notes;
                break;
            case 'Sun':
                timecard.pse__Sunday_Hours__c = hour;
                timecard.pse__Sunday_Notes__c = notes;
                break;
        }
    },

    calculateTotalHourTimecard: function (timecard) {
        const sum = Number(timecard.pse__Sunday_Hours__c) + Number(timecard.pse__Monday_Hours__c) + Number(timecard.pse__Tuesday_Hours__c) + Number(timecard.pse__Wednesday_Hours__c) + Number(timecard.pse__Thursday_Hours__c) + Number(timecard.pse__Friday_Hours__c) + Number(timecard.pse__Saturday_Hours__c);
        timecard.pse__Total_Hours__c = sum;
    },


    //calculate time for the total hours saved  / submitted / approved
    calculateTotalHoursDisplay: function (cmp, timecardsContainer) {
        let totalHours = 0;
        let totalHoursApproved = 0;
        let totalHoursSaved = 0;
        let totalHoursSubmitted = 0;
        for (let timecard of timecardsContainer) {
            switch (timecard.pse__Status__c) {
                case "Submitted":
                    totalHours += Number(timecard.pse__Total_Hours__c);
                    totalHoursSubmitted += Number(timecard.pse__Total_Hours__c);
                    break;
                case "Saved":
                    totalHours += Number(timecard.pse__Total_Hours__c);
                    totalHoursSaved += Number(timecard.pse__Total_Hours__c);
                    break;
                default:
                    totalHours += Number(timecard.pse__Total_Hours__c);
                    totalHoursApproved += Number(timecard.pse__Total_Hours__c);
                    totalHoursSubmitted += Number(timecard.pse__Total_Hours__c);
                    break;
            }
        }

        cmp.set("v.totalHoursThisWeek", totalHours);
        cmp.set("v.totalHoursUnsubmitted", totalHoursSaved);
        cmp.set("v.totalHoursSubmitted", totalHoursSubmitted);
        cmp.set("v.totalHoursApproved", totalHoursApproved);
    },

    /* updaded hours in the assignment that the timecard is being saved */
    calculateAuraAssignment: function (cmp, assignment) {
        const timecardsContainer = this.getTimecardsContainer(cmp);
        assignment.aura_total_hours_monday = 0;
        assignment.aura_total_hours_tuesday = 0;
        assignment.aura_total_hours_wednesday = 0;
        assignment.aura_total_hours_thursday = 0;
        assignment.aura_total_hours_friday = 0;
        assignment.aura_total_hours_saturday = 0;
        assignment.aura_total_hours_sunday = 0;

        for (let timecard of timecardsContainer) {
            if (timecard.pse__Assignment__c === assignment.Id) {
                assignment.aura_total_hours_monday += Number(timecard.pse__Monday_Hours__c);
                assignment.aura_total_hours_tuesday += Number(timecard.pse__Tuesday_Hours__c);
                assignment.aura_total_hours_wednesday += Number(timecard.pse__Wednesday_Hours__c);
                assignment.aura_total_hours_thursday += Number(timecard.pse__Thursday_Hours__c);
                assignment.aura_total_hours_friday += Number(timecard.pse__Friday_Hours__c);
                assignment.aura_total_hours_saturday += Number(timecard.pse__Saturday_Hours__c);
                assignment.aura_total_hours_sunday += Number(timecard.pse__Sunday_Hours__c);
            }
        }

        assignment.aura_total_hours_assignment = Number(assignment.aura_total_hours_monday) + Number(assignment.aura_total_hours_tuesday) + Number(assignment.aura_total_hours_wednesday) + Number(assignment.aura_total_hours_thursday) + Number(assignment.aura_total_hours_friday) + Number(assignment.aura_total_hours_saturday) + Number(assignment.aura_total_hours_sunday);
    },

    /* updaded milestones hours in the assignment that the timecard is being saved */
    calculateMilestoneInsideAssignment: function (cmp, currAssignment, timecard, assignmentsContainer) {
        timecard.pse__Assignment__r.pse__Role__c = currAssignment.pse__Role__c;
        const theRole = currAssignment.pse__Role__c;

        const milestones = currAssignment.milestonesOfAssignment;

        let rolesMilestone = '' //modified
        for (let milestone of milestones) {
            if (milestone.Id === timecard.pse__Milestone__c) {
                rolesMilestone = milestone.rolesMilestone;
                break;
            }
        }

        let itHasRoles = false;
        for (let roleMilestone of rolesMilestone) {
            if (roleMilestone.thisRole === theRole) {
                itHasRoles = true;
                roleMilestone.monday = Number(timecard.pse__Monday_Hours__c);
                roleMilestone.tuesday = Number(timecard.pse__Tuesday_Hours__c);
                roleMilestone.wednesday = Number(timecard.pse__Wednesday_Hours__c);
                roleMilestone.thursday = Number(timecard.pse__Thursday_Hours__c);
                roleMilestone.friday = Number(timecard.pse__Friday_Hours__c);
                roleMilestone.saturday = Number(timecard.pse__Saturday_Hours__c);
                roleMilestone.sunday = Number(timecard.pse__Sunday_Hours__c);

                roleMilestone.value = Number(roleMilestone.monday) + Number(roleMilestone.tuesday) + Number(roleMilestone.wednesday) + Number(roleMilestone.thursday) + Number(roleMilestone.friday) + Number(roleMilestone.saturday) + Number(roleMilestone.sunday)
                currAssignment.milestonesOfAssignment.rolesMilestone = rolesMilestone;
                return assignmentsContainer;
            }
        }
        if (!itHasRoles) {
            const obj = {
                'monday': Number(timecard.pse__Monday_Hours__c),
                'tuesday': Number(timecard.pse__Tuesday_Hours__c),
                'wednesday': Number(timecard.pse__Wednesday_Hours__c),
                'thursday': Number(timecard.pse__Thursday_Hours__c),
                'friday': Number(timecard.pse__Friday_Hours__c),
                'saturday': Number(timecard.pse__Saturday_Hours__c),
                'sunday': Number(timecard.pse__Sunday_Hours__c),
                'value': Number(timecard.pse__Monday_Hours__c) + Number(timecard.pse__Tuesday_Hours__c) + Number(timecard.pse__Wednesday_Hours__c) + Number(timecard.pse__Thursday_Hours__c) + Number(timecard.pse__Friday_Hours__c) + Number(timecard.pse__Saturday_Hours__c) + Number(timecard.pse__Sunday_Hours__c),
                'thisRole': theRole
            }
            rolesMilestone.push(obj);
            currAssignment.milestonesOfAssignment.rolesMilestone = rolesMilestone;
        }

        return assignmentsContainer;
    },

    //trimming unnecessary attributes in assignments to improve speed
    trimmingAssignmentsContainer: function (assignmentsContainer) {
        for (let assignment of assignmentsContainer) {
            delete assignment.attributes;
            delete assignment.pse__Bill_Rate__c;
            delete assignment.pse__Schedule__r;
            delete assignment.pse__Schedule__c;
            delete assignment.Name;
            delete assignment.CurrencyIsoCode;
            delete assignment.pse__Project__r;
            delete assignment.pse__Resource__r;
        }
        return assignmentsContainer;
    },

    trimmingMilestonesContainer: function (milestonesContainer) {
        for (let item of milestonesContainer) {
            delete item.CurrencyIsoCode;
            delete item.DisplayOrder__c;
            delete item.Parent_Milestone__c;
            delete item.RecordTypeId;
        }
        return milestonesContainer;
    },

    showToast: function (title, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": msg
        });
        toastEvent.fire();
    },

    showLoadingIndicator: function (cmp) {
        cmp.set("v.landingPageIsLoading", false);
    },

    stopLoadingIndicator: function (cmp) {
        cmp.set("v.landingPageIsLoading", true);
    },

    getAssignmentsContainer: function (cmp) {
        const assignmentContainerString = JSON.stringify(cmp.get("v.assignmentsContainer"));
        return JSON.parse(assignmentContainerString)
    },

    getTimecardsContainer: function (cmp) {
        const timecardsContainerString = JSON.stringify(cmp.get("v.timecardsContainer"));
        return JSON.parse(timecardsContainerString);
    },
    getMilestonesContainer: function (cmp) {
        const milestonesContainerString = JSON.stringify(cmp.get("v.milestonesContainer"));
        return JSON.parse(milestonesContainerString);
    },
    getChosenAssignmentId: function (cmp) {
        const assignmentChosen = cmp.get("v.entryPageAssignmentChosen");
        return assignmentChosen.Id;
    },
    getChosenMilestoneId: function (cmp) {
        const milestoneChosen = cmp.get("v.entryPageMilestoneChosen");
        return milestoneChosen.Id;
    },

    createJSONTimecard: function (timecard) {
        const timecardArray = [timecard];
        return JSON.stringify(timecardArray);
    },

    stringifyAndParseData: function (data) {
        return JSON.parse(JSON.stringify(data));
    },

    //show or hidden spinner
    toggleLoading: function (cmp) {
        cmp.set('v.landingPageIsLoading', !cmp.get('v.landingPageIsLoading'));
        return;
    },
    getDateRange: function (dateObject) {
        const date = this.getDate(dateObject);
        const month = this.getMonth(dateObject);
        const year = this.getYear(dateObject);
        const dateRangeString = month + '/' + date + '/' + year;
        return dateRangeString;
    },

    getStartAndEndDate: function (dateObject) {
        const date = this.getDate(dateObject);
        const month = this.getMonth(dateObject);
        const year = this.getYear(dateObject);
        const startAndEndDate = year + '-' + month + '-' + date;
        return startAndEndDate;
    },

    getDate: function (dateObject) {
        let dateString = dateObject.getDate().toString();
        if (dateString.length < 2) dateString = '0' + dateString;
        return dateString
    },

    getMonth: function (dateObject) {
        let monthString = (dateObject.getMonth() + 1).toString();
        if (monthString.length < 2) monthString = '0' + monthString;
        return monthString;
    },

    getYear: function (dateObject) {
        return dateObject.getFullYear().toString();
    },

    setStartDate: function (cmp, startAndEndDate) {
        const startDate = startAndEndDate[0] + 'T00:00:00-05:00';
        cmp.set("v.startDate", startDate);
    },

    setEndDate: function (cmp, startAndEndDate) {
        const endDate = startAndEndDate[6] + 'T23:59:59-05:00';
        cmp.set("v.endDate", endDate);
    },

    setStartAndEndDateList: function (cmp, startAndEndDate) {
        cmp.set("v.startAndEndDateList", startAndEndDate);
    },
})