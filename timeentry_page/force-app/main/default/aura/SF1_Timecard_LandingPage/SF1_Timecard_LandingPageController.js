({
    /* changes view from weekly to project */
    toggleTimecard: function (cmp) {
        cmp.set("v.landingPageIsShowWeekly", !cmp.get("v.landingPageIsShowWeekly"));
    },

    /* when user want to modify entry
     * set the assignment, milestone, and day for the time entry user click
     * set the hours and notes if the user has timecard, check if user is allowed to save or not (depends on timecard status) */
    modifyEntry: function (cmp, event, helper) {
        const assignmentMilestoneAndDay = event.getSource().get('v.value');
        const assignmentMilestoneAndDaySplitted = assignmentMilestoneAndDay.split(',');
        const assignmentId = assignmentMilestoneAndDaySplitted[0];
        const assignmentRole = assignmentMilestoneAndDaySplitted[3];
        helper.setAssignmentEntryPage(cmp, assignmentId, assignmentRole);

        const milestoneId = assignmentMilestoneAndDaySplitted[1];
        helper.setMilestoneEntryPage(cmp, assignmentId, milestoneId);

        const dayToModify = assignmentMilestoneAndDaySplitted[2];
        helper.changeDayDisplay(cmp, dayToModify);
        helper.setHoursAndNotes(cmp, dayToModify);

        helper.handleEntryPageDisabledSave(cmp);

        const toggleLandingPageAndEntryPage = cmp.get("v.toggleLandingPageAndEntryPage");
        $A.enqueueAction(toggleLandingPageAndEntryPage);
    },

    /* when user want to add new entry from project based view
     * set the assignment, milestone, and get the current day for the time entry user click
     * set the hours and notes if the user has timecard, check if user is allowed to save or not (depends on timecard status)  */
    addNewEntry: function (cmp, event, helper) {
        const assignmentMilestoneAndDay = event.getSource().get('v.value');
        const assignmentMilestoneAndDaySplitted = assignmentMilestoneAndDay.split(',');
        const assignmentId = assignmentMilestoneAndDaySplitted[0];
        const assignmentRole = assignmentMilestoneAndDaySplitted[2];
        helper.setAssignmentEntryPage(cmp, assignmentId, assignmentRole);

        const milestoneId = assignmentMilestoneAndDaySplitted[1];
        helper.setMilestoneEntryPage(cmp, assignmentId, milestoneId);
        const todayDate = cmp.get("v.todayDate");
        const todayDateSplitted = todayDate.toString().split(" ");
        const day = todayDateSplitted[0];
        helper.changeDayDisplay(cmp, day);
        helper.setHoursAndNotes(cmp, day);

        helper.handleEntryPageDisabledSave(cmp);
        const toggleLandingPageAndEntryPage = cmp.get("v.toggleLandingPageAndEntryPage");
        $A.enqueueAction(toggleLandingPageAndEntryPage);
    },

    /* when user want to add new entry from weekly based view
     * set the assignment, milestone, and get the current day for the time entry user click
     * set the hours and notes if the user has timecard, check if user is allowed to save or not (depends on timecard status)  */
    addNewEntryWeek: function (cmp, event, helper) {
        const assignmentMilestoneAndDay = event.getSource().get('v.value');
        const assignmentMilestoneAndDaySplitted = assignmentMilestoneAndDay.split(',');
        const assignmentId = assignmentMilestoneAndDaySplitted[0];
        const assignmentRole = assignmentMilestoneAndDaySplitted[1];

        helper.setAssignmentEntryPage(cmp, assignmentId, assignmentRole);
        helper.setMilestoneEntryPageFromWeeklyDisplay(cmp, assignmentId);
        const dayFullString = assignmentMilestoneAndDaySplitted[2];
        const day = dayFullString.slice(0, 3);
        helper.changeDayDisplay(cmp, day);
        helper.toggleDateClicked(cmp, day);

        cmp.set("v.entryPageDayChosen", day);
        helper.setHoursAndNotesFromWeekly(cmp)
        helper.handleEntryPageDisabledSave(cmp);
        helper.dontAllowEnterTimeAndNote(cmp);

        cmp.set("v.entryPageTimecardIsAlreadySubmitted", false)
        const toggleLandingPageAndEntryPage = cmp.get("v.toggleLandingPageAndEntryPage");
        $A.enqueueAction(toggleLandingPageAndEntryPage);
    },

    goToPrevWeek: function (cmp) {
        const goToPrevWeek = cmp.get("v.goToPrevWeek");
        $A.enqueueAction(goToPrevWeek);
    },

    goToNextWeek: function (cmp, helper) {
        const goToNextWeek = cmp.get("v.goToNextWeek");
        $A.enqueueAction(goToNextWeek);
    },

    /* toggle to add assignment page */
    addAssignment: function (cmp) {
        const toggleAddProjectPage = cmp.get("v.toggleAddProjectPage");
        $A.enqueueAction(toggleAddProjectPage);
    },

    submit: function (cmp) {
        const submit = cmp.get("v.submit");
        $A.enqueueAction(submit);
    }

})