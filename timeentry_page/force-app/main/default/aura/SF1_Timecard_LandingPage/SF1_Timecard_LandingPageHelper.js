({
    /* set the assignments dropdown for entry page */
    setAssignmentEntryPage: function (cmp, idAssignment, roleAssignment) {
        const assignmentsContainer = this.getAssignmentsContainer(cmp);

        const assignmentsForEntryPage = [];
        for (let assignment of assignmentsContainer) {
            assignmentsForEntryPage.push(this.generateLabelValueAssignment(assignment));

            const assignmentId = assignment.pse__Project__c;
            const assignmentRole = assignment.pse__Role__c;
            if (assignmentId === idAssignment && assignmentRole === roleAssignment) {
                cmp.set("v.entryPageAssignmentChosen", assignment);
                cmp.set("v.entryPageAssignmentChosenString", assignment.aura__assignment__name);
            }
        }

        cmp.set("v.entryPageAssignmentsContainer", assignmentsForEntryPage);
    },

    /* creating option (label + value) for assignments in entry page */
    generateLabelValueAssignment: function (assignment) {
        const assignmentObj = {};
        assignmentObj.label = assignment.aura__assignment__name;;
        assignmentObj.value = assignment.pse__Project__c + " " + assignment.pse__Role__c;
        return assignmentObj;
    },

    /* set the milestones dropdown for entry page */
    setMilestoneEntryPage: function (cmp, idAssignment, idMilestone) {
        const milestoneContainer = this.getMilestonesContainer(cmp);
        const allMilestones = [];
        for (let milestone of milestoneContainer) {
            const milestoneAssignmentId = milestone.pse__Project__c;
            const milestoneStatus = milestone.pse__Status__c;
            if (milestoneAssignmentId === idAssignment && milestoneStatus !== 'Closed') {
                allMilestones.push(this.generateLabelValueMilestone(milestone));
            }

            const milestoneId = milestone.Id
            if (milestoneId === idMilestone) {
                cmp.set("v.entryPageMilestoneChosen", milestone);
                cmp.set("v.entryPageMilestoneChosenString", milestone.Name);
            }
        }
        this.sortMilestones(allMilestones)
        cmp.set("v.entryPageMilestonesContainer", allMilestones);
    },

    //sort the milestones alphabetically
    sortMilestones: function (milestonesContainer) {
        milestonesContainer.sort(function (firstObj, secondObj) {
            let attributesForFirstObj = firstObj.label
            let attributesForSecondObj = secondObj.label;

            const lowercaseA = attributesForFirstObj.toLowerCase();
            const lowercaseB = attributesForSecondObj.toLowerCase();

            if (lowercaseA < lowercaseB)
                return -1;
            else if (lowercaseA > lowercaseB)
                return 1;
            return 0;
        });
        return;
    },

    /* creating option (label + value) for milestones in entry page */
    generateLabelValueMilestone: function (milestone) {
        const milestoneObj = {};
        if (!('Parent_Milestone__r' in milestone)) {
            milestoneObj.label = milestone.Name;
        } else {
            milestoneObj.label = milestone.Parent_Milestone__r.Name + ' - ' + milestone.Name;
        }
        milestoneObj.value = milestone.Id;
        return milestoneObj;
    },

    /* changes the indicator of day depends on the day the user choose */
    changeDayDisplay: function (cmp, day) {
        switch (day) {
            case "Sun":
                cmp.set("v.today", "Sunday");
                break;
            case "Mon":
                cmp.set("v.today", "Monday");
                break;
            case "Tue":
                cmp.set("v.today", "Tuesday");
                break;
            case "Wed":
                cmp.set("v.today", "Wednesday");
                break;
            case "Thu":
                cmp.set("v.today", "Thursday");
                break;
            case "Fri":
                cmp.set("v.today", "Friday");
                break;
            default:
                cmp.set("v.today", "Saturday");
        }
    },

    /*  change the day clicked display for the entry page,
        change the entry page hour and notes
    */
    setHoursAndNotes: function (cmp, day) {
        cmp.set("v.entryPageDayChosen", day);
        this.toggleDateClicked(cmp, day);
        this.changeEntryPageHourAndNotes(cmp, day);
    },

    /* called from add new entry in weekly version, force user to choose project, and set note and hour to default */
    setHoursAndNotesFromWeekly: function (cmp) {
        cmp.set("v.entryPageNote", "");
        cmp.set("v.entryPageHour", 0);
        this.hideMessageTimecardIsSubmitted(cmp);
        this.hideMessageTimecardIsApproved(cmp);
        cmp.set("v.entryPageMilestoneChosenString", "Select milestone:")
    },

    /* fill out all the milestones based on project clicked from weekly display*/
    setMilestoneEntryPageFromWeeklyDisplay: function (cmp, idAssignment) {
        const milestonesContainer = this.getMilestonesContainer(cmp);
        const allMilestones = [];
        for (let milestone of milestonesContainer) {
            const milestoneAssignmentId = milestone.pse__Project__c;
            const milestoneStatus = milestone.pse__Status__c;
            if (milestoneAssignmentId === idAssignment && milestoneStatus !== 'Closed') {
                allMilestones.push(this.generateLabelValueMilestone(milestone));
            }
        }

        this.sortMilestones(allMilestones);
        cmp.set("v.entryPageMilestonesContainer", allMilestones);
    },

    getTimecardsContainer: function (cmp) {
        const timecardsContainer = JSON.stringify(cmp.get("v.timecardsContainer"));
        return JSON.parse(timecardsContainer);
    },

    getAssignmentsContainer: function (cmp) {
        const assignmentContainer = JSON.stringify(cmp.get("v.assignmentsContainer"));
        return JSON.parse(assignmentContainer)
    },

    getMilestonesContainer: function (cmp) {
        const milestonesContainer = JSON.stringify(cmp.get("v.milestonesContainer"));
        return JSON.parse(milestonesContainer);
    },

    /* get date entry object
     * each object has:
     *   day: daysName
     *   click: false/true
     */
    getDayAndDate: function (cmp) {
        const dayAndDate = JSON.stringify(cmp.get("v.entryPageDaysAndDates"));
        return JSON.parse(dayAndDate);
    },

    /* changes the UI by setting the object click attribute to true depends on day */
    toggleDateClicked: function (cmp, dayChosen) {
        const dayAndentryPageDaysAndDates = this.getDayAndDate(cmp);

        for (let dayAndDate of dayAndentryPageDaysAndDates) {
            if (dayAndDate.day === dayChosen) {
                dayAndDate.click = true;
            } else {
                dayAndDate.click = false;
            }
        }
        cmp.set("v.entryPageDaysAndDates", dayAndentryPageDaysAndDates);
    },

    /* iterating through all the timecards
        set the hour and note based on the timecard (if found) */
    changeEntryPageHourAndNotes: function (cmp, dayChosen) {
        const timecardsContainer = this.getTimecardsContainer(cmp);
        const entryPageRole = this.getAssignmentChosen(cmp).pse__Role__c;
        const entryPageMilestone = this.getMilestoneChosen(cmp).Id;
        for (let timecard of timecardsContainer) {
            const timecardRole = timecard.pse__Assignment__r.pse__Role__c;
            const timecardMilestone = timecard.pse__Milestone__c;

            if (timecardRole === entryPageRole && timecardMilestone === entryPageMilestone) {
                if (timecard == null) {
                    timecard = {};
                }
                cmp.set("v.entryPageCurrentlyActiveTimecard", timecard)
                this.chooseDayToEntryPageHour(cmp, dayChosen, timecard);
                break;
            } else {
                cmp.set("v.entryPageCurrentlyActiveTimecard", {})
                this.setEntryPageHour(cmp, 0, "");
            }
        }
    },

    /* fill the data for hour and notes based on the timecard and the day */
    chooseDayToEntryPageHour: function (cmp, day, currentTimecard) {
        switch (day) {
            case 'Sun':
                this.setEntryPageHour(cmp, currentTimecard.pse__Sunday_Hours__c, currentTimecard.pse__Sunday_Notes__c);
                break;
            case 'Mon':
                this.setEntryPageHour(cmp, currentTimecard.pse__Monday_Hours__c, currentTimecard.pse__Monday_Notes__c)
                break;
            case 'Tue':
                this.setEntryPageHour(cmp, currentTimecard.pse__Tuesday_Hours__c, currentTimecard.pse__Tuesday_Notes__c)
                break;
            case 'Wed':
                this.setEntryPageHour(cmp, currentTimecard.pse__Wednesday_Hours__c, currentTimecard.pse__Wednesday_Notes__c)
                break;
            case 'Thu':
                this.setEntryPageHour(cmp, currentTimecard.pse__Thursday_Hours__c, currentTimecard.pse__Thursday_Notes__c);
                break;
            case 'Fri':
                this.setEntryPageHour(cmp, currentTimecard.pse__Friday_Hours__c, currentTimecard.pse__Friday_Notes__c);
                break;
            case 'Sat':
                this.setEntryPageHour(cmp, currentTimecard.pse__Saturday_Hours__c, currentTimecard.pse__Saturday_Notes__c)
                break;
        }
    },

    setEntryPageHour: function (cmp, hour, notes) {
        cmp.set("v.entryPageHour", hour);
        cmp.set("v.entryPageNote", notes);
    },


    /*  get the current active timecard's notes and hours based on day
        check if the timecard is saved/submitted/approved
        if submitted/approved, disable save, and display message to the user
        if saved, but the value of hours and notes is same with what the timecard has, disabled save (only allow save if the value is changed)
    */
    handleEntryPageDisabledSave: function (cmp) {
        const notes = cmp.get("v.entryPageNote");
        const hour = cmp.get("v.entryPageHour");
        let entryPageCurrentlyActiveTimecard = JSON.parse(JSON.stringify(cmp.get("v.entryPageCurrentlyActiveTimecard")));
        const today = cmp.get("v.today");
        this.hideMessageTimecardIsSubmitted(cmp);
        this.hideMessageTimecardIsApproved(cmp);
        this.allowEnterTimeAndNote(cmp);
        if (entryPageCurrentlyActiveTimecard !== null) {
            let timecardNote = '';
            let timecardHour = '';
            switch (today) {
                case 'Monday':
                    timecardNote = entryPageCurrentlyActiveTimecard.pse__Monday_Notes__c;
                    timecardHour = entryPageCurrentlyActiveTimecard.pse__Monday_Hours__c;
                    break;
                case 'Tuesday':
                    timecardNote = entryPageCurrentlyActiveTimecard.pse__Tuesday_Notes__c;
                    timecardHour = entryPageCurrentlyActiveTimecard.pse__Tuesday_Hours__c;
                    break;
                case 'Wednesday':
                    timecardNote = entryPageCurrentlyActiveTimecard.pse__Wednesday_Notes__c;
                    timecardHour = entryPageCurrentlyActiveTimecard.pse__Wednesdday_Hours__c;
                    break;
                case 'Thursday':
                    timecardNote = entryPageCurrentlyActiveTimecard.pse__Thursday_Notes__c;
                    timecardHour = entryPageCurrentlyActiveTimecard.pse__Thursday_Hours__c;
                    break;
                case 'Friday':
                    timecardNote = entryPageCurrentlyActiveTimecard.pse__Friday_Notes__c;
                    timecardHour = entryPageCurrentlyActiveTimecard.pse__Friday_Hours__c;
                    break;
                case 'Saturday':
                    timecardNote = entryPageCurrentlyActiveTimecard.pse__Saturday_Notes__c;
                    timecardHour = entryPageCurrentlyActiveTimecard.pse__Saturday_Hours__c;
                    break;
                default:
                    timecardNote = entryPageCurrentlyActiveTimecard.pse__Sunday_Notes__c;
                    timecardHour = entryPageCurrentlyActiveTimecard.pse__Sunday_Hours__c;
                    break;
            }
            if (notes === undefined || notes === "" || hour === "" || hour == 0) {
                this.dontAllowSaveEntryPage(cmp);
            } else if (notes === timecardNote && hour === timecardHour) {
                this.dontAllowSaveEntryPage(cmp);
            } else {
                this.allowSaveEntryPage(cmp);
            }
            if (entryPageCurrentlyActiveTimecard.pse__Status__c === "Submitted") {
                this.dontAllowSaveEntryPage(cmp);
                this.showMessageTimecardIsSubmitted(cmp);
                this.hideMessageTimecardIsApproved(cmp);
                this.dontAllowEnterTimeAndNote(cmp);
            }

            if (entryPageCurrentlyActiveTimecard.pse__Status__c === "Approved") {
                this.dontAllowSaveEntryPage(cmp);
                this.hideMessageTimecardIsSubmitted(cmp);
                this.showMessageTimecardIsApproved(cmp);
                this.dontAllowEnterTimeAndNote(cmp);
            }

        } else {
            if (notes === undefined || notes === "" || hour === "" || hour == 0) {
                this.dontAllowSaveEntryPage(cmp);
            } else {
                this.allowSaveEntryPage(cmp);
            }
        }
    },

    showMessageTimecardIsSubmitted: function (cmp) {
        cmp.set("v.entryPageTimecardIsAlreadySubmitted", true);
    },

    hideMessageTimecardIsSubmitted: function (cmp) {
        cmp.set("v.entryPageTimecardIsAlreadySubmitted", false);
    },

    showMessageTimecardIsApproved: function (cmp) {
        cmp.set("v.entryPageTimecardIsAlreadyApproved", true);
    },

    hideMessageTimecardIsApproved: function (cmp) {
        cmp.set("v.entryPageTimecardIsAlreadyApproved", false);
    },

    allowSaveEntryPage: function (cmp) {
        cmp.set("v.entryPageDisabledSave", false);
    },

    dontAllowSaveEntryPage: function (cmp) {
        cmp.set("v.entryPageDisabledSave", true);
    },

    allowEnterTimeAndNote: function (cmp) {
        cmp.set("v.entryPageDisabledTimeAndNotes", false);
    },

    dontAllowEnterTimeAndNote: function (cmp) {
        cmp.set("v.entryPageDisabledTimeAndNotes", true);
    },


    getAssignmentChosen: function (cmp) {
        return cmp.get("v.entryPageAssignmentChosen");
    },

    getMilestoneChosen: function (cmp) {
        return cmp.get("v.entryPageMilestoneChosen");
    },
})