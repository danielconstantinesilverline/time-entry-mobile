({
    handleAddProjectPageAllowCreateAssignment: function (cmp) {
        const addProjectPageRoleChosen = cmp.get("v.addProjectPageRoleChosen");
        const addProjectPageAssignmentChosen = cmp.get("v.addProjectPageAssignmentChosen");

        if (addProjectPageAssignmentChosen === undefined || addProjectPageRoleChosen === "--None--" || addProjectPageAssignmentChosen === "") {
            this.notAddProjectPageAllowCreateAssignmentButton(cmp);
        } else {
            this.addProjectPageAllowCreateAssignmentButton(cmp);
        }
    },

    notAddProjectPageAllowCreateAssignmentButton: function (cmp) {
        cmp.set("v.addProjectPageAllowCreateAssignment", true);
    },

    addProjectPageAllowCreateAssignmentButton: function (cmp) {
        cmp.set("v.addProjectPageAllowCreateAssignment", false);
    },

    hideDropdownProject: function (cmp) {
        cmp.set("v.addProjectPageHasListOfProjects", false);
    },

    showToast: function (title, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": msg
        });
        toastEvent.fire();
    },

    setDefaultParams: function (cmp) {
        cmp.set("v.addProjectPageAssignmentNameTyped", "");
        cmp.set("v.addProjectPageRoleChosen", "--None--");
        cmp.set("v.addProjectPageAssignmentChosen", "");
        cmp.find("selectRole").set("v.value", "--None--");
    },

    showLoadingIndicatorCreateProject: function (cmp) {
        cmp.set("v.addProjectPageIsLoading", true);
    },

    hideLoadingIndicatorCreateProject: function (cmp) {
        cmp.set("v.addProjectPageIsLoading", false);
    }
})