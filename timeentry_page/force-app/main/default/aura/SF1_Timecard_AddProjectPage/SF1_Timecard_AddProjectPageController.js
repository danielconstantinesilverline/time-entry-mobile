({
    /* get the project user choose, hide dropdown, check if user allowed to create assignmnt */
    handleProject: function (cmp, event, helper) {
        const addProjectPageAssignmentChosen = event.getSource().get("v.value");
        cmp.set("v.addProjectPageAssignmentChosen", addProjectPageAssignmentChosen);
        cmp.set("v.addProjectPageAssignmentNameTyped", event.getSource().get("v.label"))
        helper.hideDropdownProject(cmp);
        helper.handleAddProjectPageAllowCreateAssignment(cmp);
    },

    handleRole: function (cmp, event, helper) {
        const addProjectPageRoleChosen = cmp.find('selectRole').get('v.value');
        cmp.set("v.addProjectPageRoleChosen", addProjectPageRoleChosen);
        helper.handleAddProjectPageAllowCreateAssignment(cmp);
    },

    /* go back to landing page, reset every data */
    toggleAddProjectPage: function (cmp, event, helper) {
        helper.hideDropdownProject(cmp);
        cmp.set("v.showAddProjectPage", !cmp.get("v.showAddProjectPage"));
        helper.setDefaultParams(cmp);
        helper.handleAddProjectPageAllowCreateAssignment(cmp);
    },

    /* get lists of projects that contains that project name */
    onChangeTypingAssignmentName: function (cmp) {
        const addProjectPageAssignmentNameTyped = cmp.get("v.addProjectPageAssignmentNameTyped");
        const assignmentEndDate = cmp.get("v.endDate");
        const getProjectsByPartName = cmp.get("c.getProjectsByPartName");
        getProjectsByPartName.setParams({
            "partName": addProjectPageAssignmentNameTyped,
            "endDate": assignmentEndDate
        });
        getProjectsByPartName.setCallback(this, function (response) {
            const projectListString = response.getReturnValue();
            const projectList = JSON.parse(projectListString);

            const stateGetProject = response.getState();
            if (stateGetProject === 'SUCCESS') {
                projectList.map(item => {
                    item.value = item.Id;
                    item.label = item.Name;
                });
                cmp.set("v.addProjectPageHasListOfProjects", true);
                cmp.set("v.addProjectPageAssignmentsContainer", projectList);
            } else {
                console.log("Failed getting project from partial name with a state: " + stateGetProject);
            }
        });
        $A.enqueueAction(getProjectsByPartName);
    },

    /* create new assignment for the user then set all the parameter to default */
    createAssignment: function (cmp, event, helper) {
        helper.showLoadingIndicatorCreateProject(cmp);

        const createAssignment = cmp.get("c.createAssignmentAura");
        createAssignment.setParams({
            "projectId": cmp.get("v.addProjectPageAssignmentChosen"),
            "resourceId": cmp.get("v.userId"),
            "role": cmp.get("v.addProjectPageRoleChosen"),
            "startDate": cmp.get("v.startDate"),
            "endDate": cmp.get("v.endDate")
        })
        createAssignment.setCallback(this, function (response) {
            const stateCreateAssignment = response.getState();
            const returnValue = response.getReturnValue();

            if (returnValue == null) {
                helper.showToast("Failed", "You can't self-assign to this project");
            } else if (stateCreateAssignment === 'SUCCESS') {
                cmp.set("v.showAddProjectPage", !cmp.get("v.showAddProjectPage"));
                const refreshPage = cmp.get("v.refreshPage");
                $A.enqueueAction(refreshPage);
                helper.showToast("Success", "Succesfully add project");
            } else {
                console.log("Failed creating new assignment with a state: " + stateCreateAssignment);
                helper.showToast("Failed", "Failed to add project");
            }
            helper.setDefaultParams(cmp);
            helper.handleAddProjectPageAllowCreateAssignment(cmp);
            helper.hideLoadingIndicatorCreateProject(cmp);
        });
        $A.enqueueAction(createAssignment)
    },

})