declare module "@salesforce/apex/SL_ctrl_TimeEntryPage.getAssignments" {
  export default function getAssignments(param: {resource: any, startDate: any, endDate: any}): Promise<any>;
}
declare module "@salesforce/apex/SL_ctrl_TimeEntryPage.getChildMilestones" {
  export default function getChildMilestones(param: {projectId: any}): Promise<any>;
}
declare module "@salesforce/apex/SL_ctrl_TimeEntryPage.getChildMilestonesArr" {
  export default function getChildMilestonesArr(param: {projectIds: any}): Promise<any>;
}
declare module "@salesforce/apex/SL_ctrl_TimeEntryPage.getTimecards" {
  export default function getTimecards(param: {startDate: any, endDate: any, resourceId: any}): Promise<any>;
}
declare module "@salesforce/apex/SL_ctrl_TimeEntryPage.getProjectsByPartName" {
  export default function getProjectsByPartName(param: {partName: any, endDate: any}): Promise<any>;
}
declare module "@salesforce/apex/SL_ctrl_TimeEntryPage.createAssignmentAura" {
  export default function createAssignmentAura(param: {projectId: any, resourceId: any, role: any, startDate: any, endDate: any}): Promise<any>;
}
declare module "@salesforce/apex/SL_ctrl_TimeEntryPage.upsertTimecardsList" {
  export default function upsertTimecardsList(param: {objectType: any, externalIdField: any, jsonObjects: any, jsonOriginals: any}): Promise<any>;
}
declare module "@salesforce/apex/SL_ctrl_TimeEntryPage.getResourceContact" {
  export default function getResourceContact(): Promise<any>;
}
