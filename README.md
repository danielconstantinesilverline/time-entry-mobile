# README #

This is a repository for the mobile version of time entry in Silverline.

### About ###

I use Lightning component for the UI and for the business logic, I use the Apex code (SL_ctrl_TimeEntryPage.cls) that is being used for the desktop version of the time entry.

[More detail click here](https://docs.google.com/document/d/1c4Z5Ys0OItYTLrns1qfuRAINO7McymRTD0NeueHS2Ks/edit "Docs for Time Entry Mobile")
